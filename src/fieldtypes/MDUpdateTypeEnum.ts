/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright © 2023 fixparser.io
 * Released under Commercial license. Check LICENSE.md
 */
export enum MDUpdateTypeEnum {
    FullRefresh = 0,
    IncrementalRefresh = 1,
}
