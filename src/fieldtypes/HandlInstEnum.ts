/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright © 2023 fixparser.io
 * Released under Commercial license. Check LICENSE.md
 */
export enum HandlInstEnum {
    AutomatedExecutionNoIntervention = 1,
    AutomatedExecutionInterventionOK = 2,
    ManualOrder = 3,
}
