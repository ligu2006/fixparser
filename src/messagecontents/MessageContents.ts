/*
 * fixparser
 * https://gitlab.com/logotype/fixparser.git
 *
 * Copyright © 2023 fixparser.io
 * Released under Commercial license. Check LICENSE.md
 */
import prebuiltMap from '../../prebuild/MessageContentsById.prebuilt.json';
import { MappedMessageContents, Message } from '../message/Message';

export class MessageContents {
    public cacheMap: Map<number, MappedMessageContents[]> = new Map<number, MappedMessageContents[]>();
    public validated: boolean = false;

    constructor() {
        Object.entries(prebuiltMap).forEach((pair) =>
            this.cacheMap.set(Number(pair[0]), pair[1] as unknown as MappedMessageContents[]),
        );
    }

    public processMessageContents(message: Message, componentId: number): void {
        const messageContents = this.cacheMap.get(componentId);
        if (messageContents) {
            message.setMessageContents(messageContents);
        }
    }
}
