import fs from 'fs';

import { ISpecMessageContents, MESSAGE_CONTENTS } from '../spec/SpecMessageContents';
import { Components } from '../src/components/Components';
import { MappedComponents } from '../src/message/Message';

const outputPath: string = 'prebuild/';
let outputFilename: string;

const components: Components = new Components();
const mappedComponentsById: MappedComponents = {};
const mappedComponentsByTagText: MappedComponents = {};

type MessageContentsByTagText = {
    [key: string]: ISpecMessageContents[];
};

type MessageContentsByComponentId = {
    [key: string]: ISpecMessageContents[];
};

const messageContentsByTagText: MessageContentsByTagText = MESSAGE_CONTENTS.reduce(
    (groups: MessageContentsByTagText, item: ISpecMessageContents) => {
        const tagText: string = item.TagText;
        groups[tagText] = groups[tagText] || [];
        groups[tagText].push(item);
        return groups;
    },
    {},
);

const messageContentsByComponentId: MessageContentsByComponentId = MESSAGE_CONTENTS.reduce(
    (groups: MessageContentsByComponentId, item: ISpecMessageContents) => {
        const componentId: number = item.ComponentID;
        groups[componentId] = groups[componentId] || [];
        groups[componentId].push(item);
        return groups;
    },
    {},
);

console.log('Building message content (id) cache map...');
MESSAGE_CONTENTS.forEach((messageContent) => {
    const componentsById = messageContentsByComponentId[messageContent.ComponentID];
    mappedComponentsById[messageContent.ComponentID] = componentsById.map((component: ISpecMessageContents) => ({
        componentID: component.ComponentID,
        tagText: component.TagText,
        indent: component.Indent,
        position: component.Position,
        reqd: component.Reqd,
        description: component.Description,
        added: component.added,
        addedEP: component.addedEP,
        deprecated: component.deprecated,
        validated: false,
        components: components.findByName(String(component.TagText))
            ? MESSAGE_CONTENTS.filter(
                  (content) => content.ComponentID === components.findByName(String(component.TagText))!.ComponentID,
              ).map((childComponent) => ({
                  componentID: childComponent.ComponentID,
                  tagText: childComponent.TagText,
                  indent: childComponent.Indent,
                  position: childComponent.Position,
                  reqd: childComponent.Reqd,
                  description: childComponent.Description,
                  added: childComponent.added,
                  addedEP: childComponent.addedEP,
                  deprecated: childComponent.deprecated,
                  validated: false,
              }))
            : [],
    }));
});

outputFilename = `${outputPath}MessageContentsById.prebuilt.json`;
console.log(`Built message content cache map, writing to ${outputFilename}.`);

if (!fs.existsSync(outputPath)) {
    try {
        fs.mkdirSync(outputPath);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}

if (fs.existsSync(outputFilename)) {
    fs.unlinkSync(outputFilename);
}

try {
    fs.writeFileSync(outputFilename, JSON.stringify(mappedComponentsById), 'utf8');
} catch (error) {
    console.error(error);
    process.exit(2);
}

console.log('Building message content (tag text) cache map...');
MESSAGE_CONTENTS.forEach((messageContent) => {
    const componentsByTagText = messageContentsByTagText[messageContent.TagText];
    mappedComponentsByTagText[messageContent.TagText] = componentsByTagText.map((component: ISpecMessageContents) => ({
        componentID: component.ComponentID,
        tagText: component.TagText,
        indent: component.Indent,
        position: component.Position,
        reqd: component.Reqd,
        description: component.Description,
        added: component.added,
        addedEP: component.addedEP,
        deprecated: component.deprecated,
        validated: false,
        component: components.find(component.ComponentID),
    }));
});

outputFilename = `${outputPath}MessageContentsByTagText.prebuilt.json`;
console.log(`Built message content cache map, writing to ${outputFilename}.`);

if (!fs.existsSync(outputPath)) {
    try {
        fs.mkdirSync(outputPath);
    } catch (error) {
        console.error(error);
        process.exit(1);
    }
}

if (fs.existsSync(outputFilename)) {
    fs.unlinkSync(outputFilename);
}

try {
    fs.writeFileSync(outputFilename, JSON.stringify(mappedComponentsByTagText), 'utf8');
} catch (error) {
    console.error(error);
    process.exit(2);
}

console.log('Prebuild step done.');
